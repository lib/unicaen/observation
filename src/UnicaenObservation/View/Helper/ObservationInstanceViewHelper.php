<?php

namespace UnicaenObservation\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenObservation\Entity\Db\ObservationInstance;

class ObservationInstanceViewHelper extends AbstractHelper
{

    /** Liste des options :
     * - 'activer-validation' : afficher et permet de valider une observation
     * - 'can-modifier' : surcharge du privilège pour la modification
     * - 'can-historiser' : surcharge du privilège pour l'historisation/restauration
     * - 'can-supprimer' : surcharge du privilège pour la suppression
     * */
    public function __invoke(ObservationInstance $observation, array $options = []): string|Partial
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('observation-instance', ['observation' => $observation, 'options' => $options]);
    }
}
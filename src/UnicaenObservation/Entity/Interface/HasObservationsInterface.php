<?php

namespace UnicaenObservation\Entity\Interface;

use Doctrine\Common\Collections\Collection;
use UnicaenObservation\Entity\Db\ObservationInstance;
use UnicaenObservation\Entity\Db\ObservationType;

interface HasObservationsInterface
{
    public function getObservations(): Collection;
    public function hasObservation(ObservationInstance $observationInstance): bool;
    public function hasObservationWithType(ObservationType $observationType, bool $histo = false): bool;
    public function hasObservationWithTypeCode(string $code, bool $histo = false): bool;

    public function getObservationWithTypeCode(string $code, bool $histo = false): ?ObservationInstance;
    public function getObservationsWithTypeCode(string $code, bool $histo = false): array;
    public function addObservation(ObservationInstance $observationInstance): void;
    public function removeObservation(ObservationInstance $observationInstance): void;

}
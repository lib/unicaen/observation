<?php

namespace UnicaenObservation\Entity\Trait;

use Doctrine\Common\Collections\Collection;
use UnicaenObservation\Entity\Db\ObservationInstance;
use UnicaenObservation\Entity\Db\ObservationType;

trait HasObservationsTrait
{
    private Collection $observations;

    public function getObservations(): Collection
    {
        return $this->observations;
    }

    public function hasObservation(ObservationInstance $observationInstance): bool
    {
        return $this->getObservations()->contains($observationInstance);
    }

    public function hasObservationWithType(ObservationType $observationType, bool $histo = false): bool
    {
        /** @var ObservationInstance $observation */
        foreach ($this->getObservations() as $observation) {
            if ($observation->getType() === $observationType AND (!$histo OR $observation->estNonHistorise())) return true;
        }
        return false;
    }
    public function hasObservationWithTypeCode(string $code, bool $histo = false): bool
    {
        /** @var ObservationInstance $observation */
        foreach ($this->getObservations() as $observation) {
            if ($observation->getType()->getCode() === $code AND (!$histo OR $observation->estNonHistorise())) return true;
        }
        return false;
    }

    public function getObservationWithTypeCode(string $code, bool $histo = false): ?ObservationInstance
    {
        /** @var ObservationInstance $observation */
        foreach ($this->getObservations() as $observation) {
            if ($observation->getType()->getCode() === $code AND (!$histo OR $observation->estNonHistorise())) return $observation;
        }
        return null;
    }

    /** @return ObservationInstance[] */
    public function getObservationsWithTypeCode(string $code, bool $histo = false): array
    {
        $array = [];
        /** @var ObservationInstance $observation */
        foreach ($this->getObservations() as $observation) {
            if ($observation->getType()->getCode() === $code AND ($histo OR $observation->estNonHistorise())) $array[$observation->getId()] = $observation;
        }
        return $array;
    }

    public function addObservation(ObservationInstance $observationInstance): void
    {
        $this->observations->add($observationInstance);
    }

    public function removeObservation(ObservationInstance $observationInstance): void
    {
        $this->observations->removeElement($observationInstance);
    }

}
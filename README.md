Bibliothèque unicaen/observation
===


La bibliothèque **unicaen/observation** est module proposant de gérer les observations déclaration des types d'observation et mise en place des instances d'observation. 

La classe *ObservationType* 
---

Un type d'observation est constitué :
* d'un `code` unique permettant de récupérer simplement un type (idéalement regroupé dans un provider) ;
* d'un `libelle` décrivant le type de validation elle-même ; 
* d'un `categorie` décrivant un regroupe thèmatique des observations ; 

De plus les types sont munis de l'interface/trait `HistoriqueAware`.

La classe *ObservationInstance*
---

Une instance de validation est constitée :
* d'un `type` de validation (voir `ObservationType`) ;
* d'un texte `observation` portant l'observation elle-même.
    
L'interface *HasValidationsInterface* et le trait *HasValidationsTrait*  
---

Ajouter l'interface et le trait `HasObservations` va ajouter une collection `observations` (qu'il faut initialiser dans le constructeur) qui sera en charge de stocker les différentes validations.
`HasObservations` fournit les méthodes suivantes : 

```php
public function getObservations(): Collection;
public function hasObservation(ObservationInstance $observationInstance): bool;
public function hasObservationWithType(ObservationType $observationType, bool $histo = false): bool;
public function hasObservationWithTypeCode(string $code, bool $histo = false): bool;

public function getObservationWithTypeCode(string $code, bool $histo = false): ?ObservationInstance;
public function addObservation(ObservationInstance $observationInstance): void;
public function removeObservation(ObservationInstance $observationInstance): void;
```

Remarque :
___
Les instances de validation seront stockées dans la table `unicaen_observation_observation_instance` et il est nécessaire de créer en linker `entite_observation`.

Changements
===

**6.1**
- v1

Scripts et description des tables
===

Troubleshooting
===

Aucun pour le moment ...